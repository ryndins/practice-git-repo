#!/bin/bash

# Установка OpenSSH сервер
sudo apt install openssh-server -y

# Добавить ssh сервер в автозагрузку
sudo systemctl enable ssh

# Подключиться к ssh серверу по паролю
ssh ryndins@localhost

# Изменить порт для подключения в конфигурации ssh сервера
sudo vim /etc/ssh/sshd_config

# Обязательно! Перезапустить ssh сервер
sudo systemctl restart ssh

# Подключиться к ssh серверу, используя новый порт
ssh ryndins@localhost -p 2222

# Выйти / вернуться на локальную машину
exit

# Создать локальный ключ
ssh-keygen -t rsa

# Скопировать ключ на сервер
ssh-copy-id -p 2222 -i ~/.ssh/id_rsa.pub ryndins@localhost

# Зайти на сервер уже с помощью ключа
ssh ryndins@localhost -p 2222