#!/bin/bash

# Для запуска скрипта необходимо:
# 1. Перейти в директорию со скриптом
# 2. Сделать файл скрипта исполняемым. Для этого выполнить команду: chmod +x telnet.sh
# 3. Поставить перед именем скрипта "./" и после названия скрипта добавить | telnet.
# 4. Таким образом вызов будет такой: ./telnet.sh | telnet

echo "open www.postman-echo.com 80"
sleep 2
echo "GET /basic-auth HTTP/1.1"
echo "Host: www.postman-echo.com"
echo "Authorization: Basic cG9zdG1hbjpwYXNzd29yZA=="
echo
echo
sleep 2